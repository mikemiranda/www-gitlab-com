---
title: "GitLab Patch Release: x.x.x and x.x.x"
categories: release
author: ADD_YOUR_FULL_NAME
author_gitlab: GITLAB-COM_USERNAME
author_twitter: TWITTER_USERNAME
description: "Short description of the blog post"
---

Today we are releasing version X.Y.Z for GitLab Community Edition (CE) and Enterprise Editions (EES, EEP).

This version resolves a number of regressions and bugs in
[this month's x.x.w release](/YYYY/MM/DD/gitlab-x-dot-x-dot-w-released/) and
prior versions.

<!-- more -->

It includes the following fixes:

- **CE/EES/EEP:**
- **EES/EEP:**
- **Omnibus:**

## Upgrade barometer

This version does not include any new migrations, and should not require any
downtime.

Please be aware that by default the Omnibus packages will stop, run migrations,
and start again, no matter how “big” or “small” the upgrade is. This behavior
can be changed by adding a [`/etc/gitlab/skip-auto-migrations`](http://docs.gitlab.com/omnibus/update/README.html) file,
which is only used for [updates](https://docs.gitlab.com/omnibus/update/README.html).

## Updating

To update, check out our [update page](https://about.gitlab.com/update/).

## Enterprise Editions

Interested in GitLab Enterprise Editions? Check out the [features exclusive to
EE](https://about.gitlab.com/gitlab-ee/).

Access to GitLab Enterprise Editions is granted by a [subscription](https://about.gitlab.com/products/).
No time to upgrade GitLab yourself? Subscribers receive upgrade and installation
services.
